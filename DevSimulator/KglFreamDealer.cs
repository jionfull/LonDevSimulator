﻿using System;
using System.Collections.Generic;
using System.Text;
using Lon.Data.Station;
using Lon.IO.Ports;

namespace Lon.Data.Station
{
    class KglFrameDealer:FrameDealerBase
    {
        
        
        protected override DevDataFrame BuildDevFrame()
        {
            int i=0;
            for (i = 0; i < tempCache.Count; i++)
            {
                if (tempCache[i].buf[1] != i) break;
            }
            if (i != tempCache.Count) return null;
            List<byte> byteList = GetByteList(true);
            DevDataFrame txFrame = new DevDataFrame();
            txFrame.DataTime = tempCache[0].RecTime;
            txFrame.RecvTime = tempCache[0].RecTime;
            txFrame.SampleDevNo = tempCache[0].CanId >> 3;
            txFrame.DataBuf = new KglPackage[byteList.Count*8];
            for (i = 0; i < byteList.Count; i++)
            {
                for (int j = 0; j < 8; i++)
                {
                    txFrame.DataBuf[i * 8 + j] = (KglValue)((byteList[i] >> j) & 0x01);
                }
            }
            return txFrame;
        

        }
    }
}
