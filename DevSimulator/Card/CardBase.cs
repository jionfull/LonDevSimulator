﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class CardBase 
    {

        protected ChannelBase[] channels;

        public ChannelBase[] Channels
        {
            get { return channels; }
        }
        protected int cardNo;
        protected AnalogValue[] analogValues;
        protected DigitValue[] digitValues;

        public AnalogValue[] AnalogValues
        {
            get
            {
                return analogValues;
            }
        }
        
        public DigitValue[] DigitValues
        {
            get
            {
                return digitValues;
            }
        }

        public int CardNo
        {
            get { return cardNo; }
        }

        public CardBase(int cardNo)
        {
          
            this.cardNo = cardNo;
        }

        public void Init()
        {
            List<DigitValue> digits = new List<DigitValue>();
            List<AnalogValue> analogs = new List<AnalogValue>();
            for (int i = 0; i < channels.Length; i++)
            {
                for (int j = 0; j < channels[i].DigitValues.Length; j++)
                {
                    digits.Add(channels[i].DigitValues[j]);
                }
                    for (int j = 0; j < channels[i].Values.Length; j++)
                    {
                        analogs.Add(channels[i].Values[j]);
                    }
            }
            this.digitValues = digits.ToArray();
            this.analogValues = analogs.ToArray();
        }



        internal virtual void InitForm(SampleDevCardParam10 cardParam)
        {
            for(int i=0;i<cardParam.ChannelItems.Count;i++)
            {
                SampleDevChannelParam10 channelParam = cardParam.ChannelItems[i];
                ChannelBase channelBase = CreateChannelBase(channelParam);
                this.Channels[channelParam.ChannelNo] = channelBase;
            }
            this.Init();
        }

        private ChannelBase CreateChannelBase(SampleDevChannelParam10 channelParam)
        {
           switch(channelParam.ChannelType)
           {
               case 0x01:
               case 0x02:
                   return new ChannelDigit(this);
               case 16:
               case 17:
               case 18:
               case 19:
               case 20:
               case 21:
               case 23:
               case 24:
               case 25:
               case 26:
               case 27:
               case 28:
               case 29:
               case 30:
               case 31:
               case 32:
               case 33:
               case 34:
               case 35:
               case 36:
               case 37:
               case 38:
               case 39:
               case 40:
               case 41:
               case 42:
               case 44:
                        return new ChannelAC(this);
               case 45:
               case 100:
               case 101:
               case 102:
               case 103:
                        return new ChannelFSK(this);
               case 43:
                   return new ChannelTurnoutState(this);
               case 49:
               case 50:
                   return new ChannelACWithFreqPhase(this);
               case 51:
               case 52:
                   return new ChannelAC25Hz(this);
               case 148:
                   return new ChannelSemiAuto(this);
               case 255:
                    return new ChannelNull(this);
               default:
                    return new ChannelAC(this);
                  
               
           }
        }

        internal void SetValue(int analogNum, Int16 val)
        {
            
            if (analogNum < 0 || analogNum >= this.analogValues.Length)
            {
                return;
            }
            this.analogValues[analogNum].Val = val;
        }
        /// <summary>
        /// 设置开关量值
        /// </summary>
        /// <param name="index"></param>
        /// <param name="val"></param>
        internal void SetDigit(int index, byte val)
        {
            if (index < 0 || index >= this.digitValues.Length)
            {
                return;
            }
            this.digitValues[index].Val = val;

        }

        internal void SetDefaultVals(int[] vals)
        {
            for (int i = 0; i < vals.Length && i < this.analogValues.Length; i++)
            {
                if (  vals[i]!=-1)
                {
                    this.analogValues[i].Val = (Int16)vals[i];
                }
            }
        }

        internal void SetDefaultKglVals(int[] vals)
        {
            for (int i = 0; i < vals.Length && i < this.digitValues.Length; i++)
            {
                if (vals[i] != -1)
                {
                    this.digitValues[i].Val = (byte)vals[i];
                }
            }
        }
    }
}
