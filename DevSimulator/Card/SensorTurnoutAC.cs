﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class SensorTurnoutAC: CardBase
    {
        public SensorTurnoutAC(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[23];
            for (int i = 0; i < 9; i++)
            {
                base.Channels[i]=new ChannelDigit(this);
            }
            for (int i = 9; i < 23; i++)
            {
                base.Channels[i] = new ChannelNull(this);
            }
        }
    }
}
