﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class SensorTurnoutStateAC: CardBase
    {
        public SensorTurnoutStateAC(int cardNo)
            : base(cardNo)
        {
            base.channels = new ChannelBase[4];
            for (int i = 0; i < 4; i++)
            {
                base.Channels[i]=new ChannelTurnoutState(this);
            }
        }
    }
}
