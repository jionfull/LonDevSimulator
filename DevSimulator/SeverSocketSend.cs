﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace Lon.IO.Ports
{
    public  class SeverSocketSend
    {

        Thread runThread;
        Thread txThread;
        public IMessageOuter MessageOuter;
        IPEndPoint ipe = null;
        Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    
        Socket clientSocket=null;
        

        public SeverSocketSend(int port)
        {
             ipe
               = new IPEndPoint(IPAddress.Any, port);
            Start(); 
        }

        public bool Start()
        {

            runThread = new Thread(new ThreadStart(RxProc));
            runThread.IsBackground = true;
            runThread.Start();
           
            return true;
        }

  

        public  void WriteData(byte[] data)
        {
            try
            {
                if (this.clientSocket != null)
                {
                    this.clientSocket.Send(data);
                }
            }
            catch
            {
                this.clientSocket = null;
            }
      
            //for (int i = 0; i<this.netCanCardClients.Count; i++)
            //{
            //    this.netCanCardClients[i].WriteCanFrame(cf);
            //}
           
        }
        protected void RxProc()
        {

            try
            {
             
                s.Bind(ipe);
                s.Listen(100);

                while (true)
                {
                    try
                    {
                        clientSocket = s.Accept();
                  
                        OutMessage("OneConnect Is Add");
                    }
                    catch
                    {
                        Trace.WriteLine("catch");
                    }
                }
            }
            catch (System.Exception ex)
            {

            }



        }

        private void OutMessage(string mess)
        {
            if (this.MessageOuter == null) return;
            this.MessageOuter.Out(mess);
        }
       
         /// <summary>
        /// socket接收处理
        /// </summary>
        private void SocketRecive()
        {
          
            while (true)
            {
                byte[] buf = new byte[6000];
                int rxCount = clientSocket.Receive(buf);

                Thread.Sleep(1000);
                

            }
        }


        private byte[] BuildNetBuf(byte[] txBuf)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            bw.Write((byte)0x10);
            bw.Write((byte)0x02);
            for (int i = 0; i < txBuf.Length; i++)
            {
                if (txBuf[i] == 0x10)
                {
                    bw.Write((byte)0x10);
                    bw.Write((byte)0x10);
                }
                else
                {
                    bw.Write(txBuf[i]);
                }
            }
            bw.Write((byte)0x10);
            bw.Write((byte)0x03);
            return ms.ToArray();
        }

        private byte[] BuildDataBuf(CanFrame txFrame)
        {
            MemoryStream ms = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(ms);
            byte portNo = 1;
            byte devNo = (byte)(txFrame.CanId >> 3);
            if (devNo > 20 && devNo < 32)
            {
                portNo = 2;
            }
            bw.Write((byte)0x01);
            bw.Write((byte)portNo);
            bw.Write(DateTime.Now.Ticks);
            bw.Write(txFrame.CanId);
            bw.Write((byte)txFrame.Buf.Length);
            bw.Write(txFrame.Buf);
            return ms.ToArray();
        }



    }
}
