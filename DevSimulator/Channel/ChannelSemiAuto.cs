﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelSemiAuto:ChannelBase
    {


        public ChannelSemiAuto(CardBase card)
            : base(card)
        {
            this.card = card;
            this.analogValues = new AnalogValue[10];
            for (int i = 0; i < this.analogValues.Length; i++)
            {
                this.analogValues[i]=new AnalogValue();
                this.analogValues[i].MaxVal = 2000;
                this.analogValues[i].MinVal = -2000;
                this.analogValues[i].Val = 10;
            }
        }

    

    }
}
