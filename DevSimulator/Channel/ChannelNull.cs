﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.IO
{
    public class ChannelNull:ChannelBase
    {
        public ChannelNull(CardBase card):base(card)
        {
            base.analogValues = new AnalogValue[0];
        }
    }
}
