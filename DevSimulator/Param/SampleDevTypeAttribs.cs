﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Diagnostics;
using Lon.Util;
namespace Lon.IO
{
    [XmlRoot("通道类型特性")]
    public class ChannelTypeAttribFactory
    {
        [XmlIgnore]
        public static Dictionary<int, ChannelTypeAttrib> ChTypeDict = new Dictionary<int, ChannelTypeAttrib>();
        [XmlElement("通道")]
        public List<ChannelTypeAttrib> TypeAttribs = new List<ChannelTypeAttrib>();
        static ChannelTypeAttribFactory()
        {
            ChannelTypeAttribFactory attribFactory;

            XmlSerializer rserializer = new XmlSerializer(typeof(ChannelTypeAttribFactory));
            using (StreamReader reader = new StreamReader(Path.Combine(PathHelper.GetApplicationPath(),".\\ModuleCfg\\SampleDev\\TypeAttrib.Xml")))
            {
                try
                { 
                    attribFactory = rserializer.Deserialize(reader) as ChannelTypeAttribFactory;
                    foreach (ChannelTypeAttrib attrib in attribFactory.TypeAttribs)
                    {
                        ChTypeDict[attrib.TypeNo] = attrib;
                    }
                }
                catch (System.Exception ex)
                {
                    Trace.WriteLine("采集机硬件配置错误"+ex.Message);
                }
               
               
            }

        }
        static public bool Check(int chType, int cardType)
        {
            if (!ChTypeDict.ContainsKey(chType))
            {
                Trace.WriteLine("Error:ChTypeCanInCard类型不存在");
                return false;
            }
            return ChTypeDict[chType].CardType.Contains(cardType);

        }
        static public ChannelTypeAttrib GetAttrib(int type)
        {
            if (!ChTypeDict.ContainsKey(type)) return null;
            return ChTypeDict[type];
        }
    }
    public class ChannelTypeAttrib
    {
        [XmlAttribute("类型描述")]
        public String Desp;
        [XmlAttribute("类型编号")]
        public int TypeNo;
        [XmlAttribute("设置字节数")]
        public int SetBytes = 2;
        /// <summary>
        /// 当默认参考板号为0时为参考当前板号
        /// 为-1时为参考板卡开关量板
        /// </summary>
        [XmlAttribute("默认参考板号1")]
        public int RefCard1 = 0xff;
        /// <summary>
        /// 当默认参考通道为为-1时为参考板卡开关量板通道
        /// 当默认参考通道为为-2时为参考板卡为须设定阈值
        /// 为-4时为须及时 /4 为零 法则
        /// </summary>
        [XmlAttribute("默认参考通道1")]
        public int RefChanel1 = 0xff;
        [XmlAttribute("默认参考板号2")]
        public int RefCard2 = 0xff;
        [XmlAttribute("默认参考通道2")]
        public int RefChanel2 = 0xff;
        /// <summary>
        /// 当默认参数为-1时表示必须指定非零值
        /// </summary>
        [XmlAttribute("其他参数")]
        public int OtherParam = 0x00;
        [XmlElement("逻辑通道")]
        public List<LogicChannelAttrib> LogicChannels = new List<LogicChannelAttrib>();
        [XmlElement("可对应的板类型")]
        public List<int> CardType = new List<int>();

    }
    public class LogicChannelAttrib
    {
        [XmlAttribute("模拟量字节数")]
        public int AnalogByteCount=2;
        [XmlAttribute("开关量位数")]
        public int KglBitCount = 0;
        [XmlAttribute("电流曲线数")]
        public int CurveCount = 0;
        [XmlAttribute("描述")]
        public String Desp;
        [XmlAttribute("每次上传点数")]
        public int PointCount=1;

        
    }
}
