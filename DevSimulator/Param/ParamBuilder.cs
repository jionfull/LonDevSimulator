﻿using System;
using System.Collections.Generic;
using System.Text;
using Lon.IO;
using System.IO;
using Lon.Util;

namespace Lon.Data.Station
{
     class DemoHelper
    {
        static  SampleDevCfgManager demoManager = new SampleDevCfgManager();
        static Dictionary<int,SampleDevCardParam10> cardTypeIdDict=new Dictionary<int,SampleDevCardParam10>();
        static Dictionary<string, SampleDevCardParam10> cardTypeNameDict = new Dictionary<string, SampleDevCardParam10>();
        static Dictionary<String, int> idNameDict = new Dictionary<string, int>();

        static DemoHelper()
        {
            String appPath = PathHelper.GetApplicationPath();
            demoManager.LoadFormXml(Path.Combine(appPath,@"ModuleCfg\SampleDev\采集机配置模板.xml"));
            IniDoc cfg = new IniDoc(Path.Combine(appPath,@"ModuleCfg\SampleDev\Id名称对照表.ini"));
            cfg.Load();
            if (demoManager.Items == null || demoManager.Items.Count == 0 || demoManager.Items[0]==null)
            {
               
                return;
            } 
            List<SampleDevCardParam10> cards = demoManager.Items[0].CardItems;
            if(cards==null||cards.Count==0)
            {
                return;
            }
            for(int i=0;i<cards.Count;i++)
            {
                if (cards[i] == null) continue;
                if (!cardTypeIdDict.ContainsKey(cards[i].CardNo))
                {
                   cardTypeIdDict.Add(cards[i].CardNo, cards[i]);
                }
               
                String typeName = cfg.GetString("板卡类型", cards[i].CardNo.ToString());
                if (String.IsNullOrEmpty(typeName)) continue;
                if (!cardTypeNameDict.ContainsKey(typeName))
                {
                   cardTypeNameDict.Add(typeName,cards[i]);
                }
                        
            }
           
        }
         
        public static SampleDevCardParam10 BuildCard(int typeId,int cardNo)
        {
            if (!cardTypeIdDict.ContainsKey(typeId))
            {
                return null;
            }
            SampleDevCardParam10 sdcp = new SampleDevCardParam10(cardTypeIdDict[typeId]);
            sdcp.CardNo = cardNo;
            for(int i=0;i<sdcp.ChannelItems.Count;i++)
            {
                if(sdcp.ChannelItems[i].RelatedCard1==-2)
                {
                    sdcp.ChannelItems[i].RelatedCard1 = cardNo;
                }
            }
            return sdcp;
          
        }

        public static SampleDevCardParam10 BuildCard(int typeId, int cardNo,int refCardNo,ref int refCh)
        {
            if (!cardTypeIdDict.ContainsKey(typeId)) return null;
            SampleDevCardParam10 sdcp = new SampleDevCardParam10(cardTypeIdDict[typeId]);
            sdcp.CardNo = cardNo;
           
            for (int i = 0; i < sdcp.ChannelItems.Count; i++)
            {
                if (sdcp.ChannelItems[i].RelatedCard1 == -2)
                {
                    sdcp.ChannelItems[i].RelatedCard1 = cardNo;
                }
                if(sdcp.ChannelItems[i].RelatedCard1==-1)
                {
                    sdcp.ChannelItems[i].RelatedCard1 = refCardNo;
                    sdcp.ChannelItems[i].RelatedCard1 = refCh++;
                }
            }
            if(refCh>47)
            {
                return null;
            }

            return sdcp;

        }
    }
}
