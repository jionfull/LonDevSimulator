﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Lon.IO.Ports;

namespace Lon.Data.Station
{
    abstract class FrameDealerBase
    {
        public List<CanFrame> tempCache = new List<CanFrame>();
        public List<DevDataFrame> txFramesCache = new List<DevDataFrame>();
        public virtual void DealCanFrame(CanFrame frame)
        {
            if (frame == null)
            {
                Trace.WriteLine("Waning:  程序运行中出现问题,开关量处理收到空帧");
                return;
            }
            if (frame.buf[0] == 0x10)
            {
                tempCache.Clear();
                tempCache.Add(frame);
            }
            else if (frame.buf[0] == 0x30)
            {
                tempCache.Add(frame);
                int i;
                 DevDataFrame txFrame = BuildDevFrame();
                 if (txFrame != null)
                 {
                     txFramesCache.Add(txFrame);
                     if (txFramesCache.Count > 0) txFramesCache.RemoveAt(0);
                     DevDataPort.Write(txFrame);
                 }
             
                tempCache.Clear();
            }
            else
            {
                tempCache.Add(frame);
            }
        }
        protected abstract DevDataFrame BuildDevFrame();
        protected List<byte> GetByteList(bool haveFrameIndex)
        {
            List<byte> ret = new List<byte>(1000);
            int dataStartInFrame = 1;
            if (haveFrameIndex)
            {
                dataStartInFrame = 2;
            }
            for (int i = 0; i < tempCache.Count; i++)
            {
                for (int j = dataStartInFrame; j< tempCache[i].buf.Length; j++)
                {
                    ret.Add(tempCache[i].buf[j]);
                }
            }
            return ret;
        }
        
    }
}
