﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Data.Station
{
    /// <summary>
    /// 下位机校时用的Helper类
    /// </summary>
    internal class UnixTimeTicksHelper
    {
      
           static readonly DateTime StartTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));

            public static DateTime FromLong(long time)
            {
                return StartTime.AddSeconds(time*0.01);
            }
            public static long ToLong(DateTime time)
            {
                return (long)(((time - StartTime).TotalSeconds)*100);
            }
    }
}
