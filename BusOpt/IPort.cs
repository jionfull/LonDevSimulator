﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Lon.IO.Ports
{
    public interface IPort<T>
    {
        string PortName { get; set; }
        bool Open();
        bool IsOpen();
        bool Close();
       // void Write(T val);
        AutoResetEvent RxEvent { get; }
        bool Read(ref T val, int timeOut);
        bool Read(ref T val);
        int BufLen { get; set; }
        void ClrBuffer();
    }
}
