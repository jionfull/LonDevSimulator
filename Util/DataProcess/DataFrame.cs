﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Data.DataProcess
{
    public class DataFrame<T>
    {
        /// <summary>
        ///  数据帧类型
        /// </summary>
        private T frameType;

        /// <summary>
        /// 数据帧类型
        /// </summary>
        public virtual T FrameType
        {
            get { return frameType; }
            set { frameType = value; }
        }

        private byte[] buf;

        public DataFrame(byte[] buf,T frameType)
        {
            this.frameType = frameType;
            this.buf = buf;
        }

        /// <summary>
        /// 数据内容
        /// </summary>
        public byte[] Buf
        {
            get { return buf; }
            set { buf = value; }
        }
       

    }
}
