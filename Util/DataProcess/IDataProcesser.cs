﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lon.Data.DataProcess
{
    /// <summary>
    /// 数据处理器
    /// </summary>
   public interface IDataProcesser<T>
    {
        /// <summary>
        /// 返回执行是否成功
        /// </summary>
        /// <param name="frame"></param>
        /// <returns></returns>
        bool Process(DataFrame<T> frame);
    }
}
